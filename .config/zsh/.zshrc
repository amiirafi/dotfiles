# Enable Colors
autoload -U colors && colors

# Aliases
# Aliases
alias ps='procs'
alias cat='bat'
alias sudo='doas'
alias sudoedit='doas rnvim'
alias ls='exa -alh --color=always --group-directories-first'
alias grep='grep --color=auto'
alias kill='killall -q'
alias v='nvim'

alias pacup='sudo pacman -Syu'

alias cp="cp -i"
alias mv='mv -i'
alias rm='rm -i'
alias mkdir='mkdir -pv'

# History in cache directory
HISTSIZE=1000
SAVEHIST=1000
HISTFILE=~/.cache/zsh/history

# Basic Tab/Auto Completion
autoload -U compinit && compinit -u
zstyle ':completion:*' menu select

# Auto complete with case insenstivity
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'

zmodload zsh/complist
compinit
_comp_options+=(globdots)

# Load zsh-syntax-highlighting
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null

# Load Auto-Suggestions
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh 2>/dev/null
# Search repos for programs that can't be found
source /usr/share/doc/pkgfile/command-not-found.zsh 2>/dev/null

# Starship Prompt
eval "$(starship init zsh)"
